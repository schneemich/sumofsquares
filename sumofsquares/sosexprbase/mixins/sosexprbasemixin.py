import functools
import polymatrix

from sumofsquares.sosexprbase.mixins.exprbasemixin import ExprBaseMixin


class SOSExprBaseMixin(ExprBaseMixin):
    @functools.cached_property
    def sos_monom(self) -> polymatrix.Expression:
        return self.expr.quadratic_monomials(self.variables).cache()

    @functools.cached_property
    def sos_matrix(self) -> polymatrix.Expression:
        sos_matrix = self.expr.quadratic_in(
            variables=self.variables, 
            monomials=self.sos_monom,
        ).symmetric().cache()

        return sos_matrix

    @property
    def sos_matrix_vec(self) -> polymatrix.Expression:
        return self.sos_matrix.reshape(-1, 1)
