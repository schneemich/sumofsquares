import abc
import polymatrix


class ParameterMixin(abc.ABC):
    """
    
    """

    @property
    @abc.abstractmethod
    def name(self) -> str:
        ...

    @property
    @abc.abstractmethod
    def param(self) -> polymatrix.Expression:
        ...

    @property
    @abc.abstractmethod
    def monom(self) -> polymatrix.Expression:
        ...
