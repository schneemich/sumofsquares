import abc

from sumofsquares.sosexprbase.mixins.parametermixin import ParameterMixin

class DependenceMixin(abc.ABC):
    @property
    @abc.abstractmethod
    def dependence(self) -> tuple[ParameterMixin]:
        ...

