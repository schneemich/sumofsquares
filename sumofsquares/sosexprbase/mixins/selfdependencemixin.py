import abc

from sumofsquares.sosexprbase.mixins.dependencemixin import DependenceMixin
from sumofsquares.sosexprbase.mixins.parametermixin import ParameterMixin

class SelfDependenceMixin(DependenceMixin):
    @property
    def dependence(self) -> set[ParameterMixin]:
        return (self,)
