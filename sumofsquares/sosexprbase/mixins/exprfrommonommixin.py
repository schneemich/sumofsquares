import abc
import polymatrix

from sumofsquares.sosexprbase.mixins.exprbasemixin import ExprBaseMixin
from sumofsquares.sosexprbase.mixins.parametermixin import ParameterMixin


class ExprFromMonomMixin(ParameterMixin, ExprBaseMixin):
    @property
    @abc.abstractmethod
    def param_matrix(self) -> polymatrix.Expression:
        ...

    @property
    @abc.abstractmethod
    def n_row(self) -> int:
        ...

    @property
    def expr(self) -> polymatrix.Expression:
        expr_vec = (self.param_matrix @ self.monom).cache()

        if self.n_row == 1:
            return expr_vec
        
        else:
            return expr_vec.reshape(self.n_row, -1)

