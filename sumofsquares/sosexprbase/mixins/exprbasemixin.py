import abc
import polymatrix

from sumofsquares.sosexprbase.mixins.dependencemixin import DependenceMixin


class ExprBaseMixin(DependenceMixin):
    @property
    @abc.abstractmethod
    def expr(self) -> polymatrix.Expression:
        ...

    @property
    @abc.abstractmethod
    def variables(self) -> polymatrix.Expression:
        ...
