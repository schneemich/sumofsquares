import polymatrix

from sumofsquares.sosexprbase.impl import ParamSOSExprBaseImpl, SOSExprBaseImpl
from sumofsquares.sosexprbase.mixins.parametermixin import ParameterMixin


def init_sos_expr_base(
    expr: polymatrix.Expression, 
    variables: polymatrix.Expression,
    dependence: tuple[ParameterMixin] | None = None,
):
    
    if not isinstance(expr, polymatrix.Expression):
        expr = polymatrix.from_(expr)

    # if variables is None:
    #     variables = polymatrix.from_(1)

    if dependence is None:
        dependence = tuple()

    return SOSExprBaseImpl(
        expr=expr,
        variables=variables,
        dependence=dependence,
    )


def init_param_sos_expr_base(
    name: str, 
    variables: polymatrix.Expression,
    monom: polymatrix.Expression | None = None,
    n_row: int | None = None,
    n_col: int | None = None,
):
    if monom is None:
        monom = polymatrix.from_(1)

    if n_row == None:
        n_row = 1

    if n_col == None:
        n_col = 1

    if n_row == 1 and n_col == 1:
        param = monom.parametrize(f'{name}')
        param_matrix = param.T

    else:
        param = monom.rep_mat(n_col * n_row, 1).parametrize(f'{name}')
        param_matrix = param.reshape(monom, -1).T

        # params = tuple(monom.parametrize(f'{name}_{row+1}_{col+1}') for col in range(n_col) for row in range(n_row))
        # param = polymatrix.v_stack(params)
        # param_matrix = polymatrix.v_stack(tuple(param.T for param in params))

    return ParamSOSExprBaseImpl(
        name=name,
        param=param,
        monom=monom,
        variables=variables,
        param_matrix=param_matrix,
        n_row=n_row,
    )
