import dataclassabc
import polymatrix

from sumofsquares.sosexprbase.abc import ParamSOSExprBase, SOSExprBase
from sumofsquares.sosexprbase.mixins.parametermixin import ParameterMixin


@dataclassabc.dataclassabc(frozen=True)
class SOSExprBaseImpl(SOSExprBase):
    expr: polymatrix.Expression
    variables: polymatrix.Expression
    dependence: tuple[ParameterMixin]


@dataclassabc.dataclassabc(frozen=True)
class ParamSOSExprBaseImpl(ParamSOSExprBase):
    name: str
    param: polymatrix.Expression
    monom: polymatrix.Expression
    variables: polymatrix.Expression
    param_matrix: polymatrix.Expression
    n_row: int
