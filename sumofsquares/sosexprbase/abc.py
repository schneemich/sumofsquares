import abc

from sumofsquares.sosexprbase.mixins.selfdependencemixin import SelfDependenceMixin
from sumofsquares.sosexprbase.mixins.sosexprbasemixin import SOSExprBaseMixin
from sumofsquares.sosexprbase.mixins.exprfrommonommixin import ExprFromMonomMixin


class SOSExprBase(SOSExprBaseMixin, abc.ABC):
    pass


class ParamSOSExprBase(SelfDependenceMixin, SOSExprBaseMixin, ExprFromMonomMixin, abc.ABC):
    pass
