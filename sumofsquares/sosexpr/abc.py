import abc

from sumofsquares.sosexpr.mixins.parametermixin import ParamSOSExprMixin
from sumofsquares.sosexpr.mixins.sosexpropmixin import SOSExprOPMixin


class SOSExpr(SOSExprOPMixin, abc.ABC):
    pass


class ParamSOSExpr(SOSExprOPMixin, ParamSOSExprMixin, abc.ABC):
    pass
