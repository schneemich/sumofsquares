import abc

from sumofsquares.sosexpr.abc import SOSExpr


class GetSOSConstraintMixin(abc.ABC):
    @property
    @abc.abstractmethod
    def sos_constraints(self) -> tuple[SOSExpr]:
        ...
