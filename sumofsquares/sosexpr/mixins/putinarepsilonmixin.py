import abc

from sumofsquares.sosexpr.abc import ParamSOSExpr, SOSExpr
from sumofsquares.sosexpr.mixins.getsosconstraintmixin import GetSOSConstraintMixin


class PutinarEpsilonMixin(GetSOSConstraintMixin, abc.ABC):
    @property
    @abc.abstractmethod
    def name(self) -> str:
        ...

    @property
    @abc.abstractmethod
    def epsilon(self) -> ParamSOSExpr:
        ...

    @property
    @abc.abstractmethod
    def gamma(self) -> dict[str, ParamSOSExpr]:
        ...

    @property
    @abc.abstractmethod
    def condition(self) -> SOSExpr:
        ...
