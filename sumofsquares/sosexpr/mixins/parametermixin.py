import abc
import polymatrix

from sumofsquares.sosexpr.mixins.sosexprmixin import SOSExprMixin
from sumofsquares.sosexprbase.abc import ParamSOSExprBase
from sumofsquares.sosexprbase.mixins.parametermixin import ParameterMixin


class ParamSOSExprMixin(ParameterMixin, SOSExprMixin):
    @property
    @abc.abstractmethod
    def underlying(self) -> ParamSOSExprBase:
        ...

    @property
    def name(self) -> polymatrix.Expression:
        return self.underlying.name

    @property
    def param(self) -> polymatrix.Expression:
        return self.underlying.param

    @property
    def monom(self) -> polymatrix.Expression:
        return self.underlying.monom
