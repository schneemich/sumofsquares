import dataclasses
import typing

from numpy import isin

import polymatrix
import polymatrix.expression.from_

from sumofsquares.sosexpr.mixins.sosexprmixin import SOSExprMixin
from sumofsquares.sosexprbase.init import init_sos_expr_base


class SOSExprOPMixin(SOSExprMixin):
    @staticmethod
    def _binary(
        op, 
        left: 'SOSExprOPMixin', 
        right: typing.Union[polymatrix.Expression, 'SOSExprOPMixin'],
    ) -> 'SOSExprOPMixin':

        if not isinstance(left, SOSExprOPMixin):
            if isinstance(left, tuple):
                return NotImplemented
    
            left = polymatrix.expression.from_.from_expr_or_none(left)

            if left is None:
                return NotImplemented

            underlying = init_sos_expr_base(
                expr=op(polymatrix.from_(left), right.expr),
                variables=right.variables,
                dependence=right.dependence,
            )

            return dataclasses.replace(
                right,
                underlying=underlying,
            )

        if not isinstance(right, SOSExprOPMixin):
            if isinstance(right, tuple):
                return NotImplemented

            right = polymatrix.expression.from_.from_expr_or_none(right)

            if right is None:
                return NotImplemented
            
            underlying = init_sos_expr_base(
                expr=op(left.expr, polymatrix.from_(right)),
                variables=left.variables,
                dependence=left.dependence,
            )

        else:
            # var_set_left = set(left.variables)
            # var_set_right = set(right.variables)
            # assert var_set_left.issubset(var_set_right) or var_set_right.issubset(var_set_left), f'{left.variables=}, {right.variables=}'

            assert left.variables == right.variables, f'{left.variables=}, {right.variables=}'

            underlying=init_sos_expr_base(
                expr=op(left.expr, right.expr),
                variables=left.variables,
                dependence=tuple(set(left.dependence + right.dependence)),
            )

        return dataclasses.replace(
            left,
            underlying=underlying,
        )

    @staticmethod
    def _unary(op, expr: 'SOSExprOPMixin') -> 'SOSExprOPMixin':
        return dataclasses.replace(
            expr,
            underlying=init_sos_expr_base(
                expr=op(expr.expr),
                variables=expr.variables,
                dependence=expr.dependence,
            ),
        )

    def __add__(self, other: typing.Union[polymatrix.Expression, 'SOSExprOPMixin']) -> 'SOSExprOPMixin':
        return self._binary(polymatrix.Expression.__add__, self, other)

    def __matmul__(self, other: typing.Union[polymatrix.Expression, 'SOSExprOPMixin']) -> 'SOSExprOPMixin':
        return self._binary(polymatrix.Expression.__matmul__, self, other)

    def __mul__(self, other: typing.Union[polymatrix.Expression, 'SOSExprOPMixin']) -> 'SOSExprOPMixin':
        return self._binary(polymatrix.Expression.__mul__, self, other)
    
    def __neg__(self) -> 'SOSExprOPMixin':
        return self._unary(polymatrix.Expression.__neg__, self)

    def __radd__(self, other):
        return self._binary(polymatrix.Expression.__add__, self, other)

    def __rmatmul__(self, other: typing.Union[polymatrix.Expression, 'SOSExprOPMixin']) -> 'SOSExprOPMixin':
        return self._binary(polymatrix.Expression.__matmul__, other, self)
    
    def __rmul__(self, other):
        return self._binary(polymatrix.Expression.__mul__, self, other)

    def __rsub__(self, other: typing.Union[polymatrix.Expression, 'SOSExprOPMixin']) -> 'SOSExprOPMixin':
        return self._binary(polymatrix.Expression.__sub__, other, self)

    def __sub__(self, other: typing.Union[polymatrix.Expression, 'SOSExprOPMixin']) -> 'SOSExprOPMixin':
        return self._binary(polymatrix.Expression.__sub__, self, other)

    def __getitem__(self, key: tuple[int, int]):
        return dataclasses.replace(
            self,
            underlying=init_sos_expr_base(
                expr=self.expr[key[0], key[1]],
                variables=self.variables,
                dependence=self.dependence,
            ),
        )

    def cache(self) -> 'SOSExprOPMixin':
        return self._unary(polymatrix.Expression.cache, self)

    def diff(
        self, 
        variables: polymatrix.Expression | None = None,
    ) -> 'SOSExprOPMixin':
        if variables is None:
            variables = self.variables

        return dataclasses.replace(
            self,
            underlying=init_sos_expr_base(
                expr=self.expr.diff(variables),
                variables=self.variables,
                dependence=self.dependence,
            ),
        )
    
    def divergence(
        self, 
        variables: polymatrix.Expression | None = None,
    ) -> 'SOSExprOPMixin':
        if variables is None:
            variables = self.variables

        return dataclasses.replace(
            self,
            underlying=init_sos_expr_base(
                expr=self.expr.divergence(variables),
                variables=self.variables,
                dependence=self.dependence,
            ),
        )

    @property
    def T(self):
        return self._unary(polymatrix.Expression.transpose, self)

    def substitute(self, substitutions, variables):
        return dataclasses.replace(
            self,
            underlying=init_sos_expr_base(
                expr=self.expr.substitute(substitutions),
                # variables=self.variables.substitute(substitutions),
                variables=variables,
                dependence=self.dependence,
            ),
        )
    
    def set_variables(self, variables):
        return dataclasses.replace(
            self,
            underlying=init_sos_expr_base(
                expr=self.expr,
                variables=variables,
                dependence=self.dependence,
            ),
        )

    def v_stack(self, other):
        def op(left, right):
            return polymatrix.v_stack((left, right))

        return self._binary(op, self, other)
