import abc
import polymatrix

from sumofsquares.sosexprbase.abc import SOSExprBase


class SOSExprMixin(abc.ABC):
    @property
    @abc.abstractmethod
    def underlying(self) -> SOSExprBase:
        ...

    @property
    def expr(self) -> polymatrix.Expression:
        return self.underlying.expr

    @property
    def variables(self) -> polymatrix.Expression:
        return self.underlying.variables

    @property
    def dependence(self) -> polymatrix.Expression:
        return self.underlying.dependence

    @property
    def sos_matrix(self) -> polymatrix.Expression:
        return self.underlying.sos_matrix

    @property
    def sos_matrix_vec(self) -> polymatrix.Expression:
        return self.underlying.sos_matrix_vec
