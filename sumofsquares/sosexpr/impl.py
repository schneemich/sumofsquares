import dataclassabc

from sumofsquares.sosexprbase.abc import ParamSOSExprBase, SOSExprBase
from sumofsquares.sosexpr.mixins.putinarepsilonmixin import PutinarEpsilonMixin
from sumofsquares.sosexpr.abc import ParamSOSExpr, SOSExpr


@dataclassabc.dataclassabc(frozen=True)
class SOSExprImpl(SOSExpr):
    underlying: SOSExprBase


@dataclassabc.dataclassabc(frozen=True)
class ParamSOSExprImpl(ParamSOSExpr):
    underlying: ParamSOSExprBase

    def __eq__(self, other):
        if isinstance(other, ParamSOSExprImpl):
            return self.underlying == other.underlying
        
        elif isinstance(other, ParamSOSExprBase):
            return self.underlying == other
        
        else:
            return False
    
    def __hash__(self):
        return hash(self.underlying)


@dataclassabc.dataclassabc(frozen=True)
class PutinarEpsilonImpl(PutinarEpsilonMixin):
    name: str
    epsilon: ParamSOSExpr
    gamma: dict[str, ParamSOSExpr]
    sos_constraints: tuple[SOSExpr]
    condition: SOSExpr
