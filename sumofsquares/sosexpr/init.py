import polymatrix

from sumofsquares.sosexprbase.mixins.parametermixin import ParameterMixin
from sumofsquares.sosexprbase.init import init_param_sos_expr_base, init_sos_expr_base
from sumofsquares.sosexpr.mixins.sosexprmixin import SOSExprMixin
from sumofsquares.sosexpr.abc import SOSExpr
from sumofsquares.sosexpr.impl import ParamSOSExprImpl, SOSExprImpl, PutinarEpsilonImpl


def init_sos_expr(
    expr: polymatrix.Expression, 
    variables: polymatrix.Expression,
    dependence: tuple[ParameterMixin],
):
    return SOSExprImpl(
        underlying=init_sos_expr_base(
            expr=expr,
            variables=variables,
            dependence=dependence,
        ),
    )


def init_param_expr(
    name: str, 
    variables: polymatrix.Expression,
    monom: polymatrix.Expression | None = None,
    n_row: int | None = None,
    n_col: int | None = None,
):
    return ParamSOSExprImpl(
        underlying=init_param_sos_expr_base(
            name=name,
            monom=monom,
            variables=variables,
            n_row=n_row,
            n_col=n_col,
        ),
    )


def init_param_expr_from_reference(
    name: str, 
    reference: SOSExpr,
    # variables: polymatrix.Expression,
    multiplicand: SOSExpr | polymatrix.Expression | None = None,
):
    variables = reference.variables

    if multiplicand is None:
        multiplicand_expr = polymatrix.from_(1)

    elif isinstance(multiplicand, polymatrix.Expression):
        multiplicand_expr = multiplicand

    elif isinstance(multiplicand, SOSExprMixin):
        assert multiplicand.variables == variables, f'{multiplicand.variables=}, {variables=}'

        multiplicand_expr = multiplicand.expr

    else:
        multiplicand_expr = polymatrix.from_(multiplicand)

    m_sos_monom = multiplicand_expr.quadratic_monomials(variables)
    
    max_degree = m_sos_monom.degree().T.max()
    
    m_max_monom = m_sos_monom.filter(
        m_sos_monom.degree() - max_degree, 
        inverse=True,
    )
    
    sos_monom = reference.expr.quadratic_monomials(variables).subtract_monomials(m_max_monom)

    expr = (sos_monom @ sos_monom.T).reshape(1, -1).sum()

    monom = expr.linear_monomials(variables).cache()

    return init_param_expr(
        name=name,
        monom=monom,
        variables=variables,
    )


def init_putinar_epsilon(
    name: str,
    f0: SOSExpr,
    fi: dict[str, SOSExpr | polymatrix.Expression] | None = None,
    gi: dict[str, SOSExpr | polymatrix.Expression] | None = None,
    epsilon_min: SOSExpr | None = None,
    decrease_rate: SOSExpr | polymatrix.Expression | None = None,
):
    sos_constraints = tuple()

    condition = f0

    def gen_gamma(fi):
        for key, val in fi.items():
            yield key, init_param_expr_from_reference(
                name=f'gamma_{key}_{name}',
                reference=f0,
                multiplicand=val,
            )

    if fi is None:
        gamma_fi = {}

    else:
        gamma_fi = dict(gen_gamma(fi))

        for key, gamma_i in gamma_fi.items():
            sos_constraints += (gamma_i,)
            condition = condition - gamma_i * fi[key]

    if gi is None:
        gamma_gi = {}

    else:
        gamma_gi = dict(gen_gamma(gi))

        for key, gamma_i in gamma_gi.items():
            condition = condition - gamma_i * gi[key]

    if epsilon_min is None:
        epsilon = None

    else:
        epsilon = init_param_expr(
            name=f'epsilon_{name}',
            variables=f0.variables,
        )

        sos_constraints += (epsilon - epsilon_min,)
        condition += epsilon

    if decrease_rate is not None:
        condition -= decrease_rate

    sos_constraints += (condition,)

    return PutinarEpsilonImpl(
        name=name,
        epsilon=epsilon,
        gamma=gamma_fi | gamma_gi,
        sos_constraints=sos_constraints,
        condition=condition,
    )
