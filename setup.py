from setuptools import setup, find_packages

setup(
    name='sumofsquares',
    packages=find_packages(include=['sumofsquares', 'sumofsquares.*']),
    version='0.0.0',
)